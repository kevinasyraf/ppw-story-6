from django.urls import path
from .views import *

app_name = 'statuslist'

urlpatterns = [
    path('', statuslist, name='statuslist'),
    path('add_status/',add_status, name='add_status'),
]