from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.

def statuslist(request):
    form = StatusForm()
    status = Status.objects.all()
    context = {
        'form': form,
        'status': status
    }
    return render(request, 'index.html', context)


def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            new_status = Status.objects.create(status = request.POST['status'])
        return redirect('/')