from django.test import TestCase
from django.urls import resolve
from .views import statuslist
from .models import Status
import datetime

from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import unittest
import time
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class LandingPageUnitTest(TestCase):

    def test_landing_page_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_statuslist_function(self):
        response = resolve('/')
        self.assertEqual(response.func, statuslist)

    def test_landing_page_using_statuslist_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_model_can_create_new_status(self):
        status_baru = Status.objects.create(status='tes gan')
        jumlah_status_baru = Status.objects.all().count()
        self.assertEqual(jumlah_status_baru, 1)

    def test_model_can_create_new_status_with_status_as_title(self):
        status_baru = Status.objects.create(status='tes gan')
        self.assertEqual(str(status_baru), status_baru.status)

    def test_can_save_status_POST_request(self):
        response = self.client.post('/add_status/', data={
            'status' : 'haha hehe'
        })
        jumlah_status_baru = Status.objects.all().count()
        self.assertEqual(jumlah_status_baru, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_new_status_shown_in_website(self):
        response = self.client.post('/add_status/', data={
            'status' : 'haha hehe'
        })
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('haha hehe', html_response)

class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        op = Options()
        op.add_argument('--dns-prefetch-disable')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=op)
        return super().setUp()

    def tearDown(self):
        self.browser.quit()
        return super().tearDown()
        

    def test_input_a_status_and_make_it_shown(self):
        self.browser.get('%s%s' % (self.live_server_url, '/'))
        time.sleep(2)
        input_box = self.browser.find_element_by_id('id_status')
        input_box.send_keys('haha hehe')
        input_box.submit()
        time.sleep(2)
        self.assertIn('haha hehe', self.browser.page_source)